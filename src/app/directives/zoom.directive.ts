import { Directive, ElementRef } from "@angular/core";

@Directive({
    selector: '[zoom]',
})

export class ZoomDirective {
    constructor(private el: ElementRef) {
      el.nativeElement.style.fontSize = '12px'
    }
}