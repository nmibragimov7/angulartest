import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { ProductComponent } from './components/product/product.component';
import { CartService } from './services/cart.service';
import { CartComponent } from './components/cart/cart.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { CutTextPipe } from './pipes/cut-text.pipe';
import { ZoomDirective } from './directives/zoom.directive';
import { ProductsService } from './services/products.service';
import { ToastComponent } from './components/base/toast/toast.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductDetailsComponent,
    ToolbarComponent,
    ProductComponent,
    CartComponent,
    AddProductComponent,
    CutTextPipe,
    ZoomDirective,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    // CartService,
    // ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
