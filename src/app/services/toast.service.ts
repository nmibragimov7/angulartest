import { Injectable, TemplateRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  toasts: any[] = [];

  show(id: string, textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ id, textOrTpl, ...options });
  }

  remove(id: string) {
    const index = this.toasts.findIndex(item => {
      return item.id === id;
    });

    if (index > -1) {
      this.toasts.splice(index, 1);
    }
  }
}
