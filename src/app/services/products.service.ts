import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface Product {
  id: any;
  userId: any;
  title: any;
  body: any;
}

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  products: Product[] = [];
  error: string = '';
  isLoading: boolean = false;

  constructor(
    private http: HttpClient
  ) { }

  getProducts() {
    return this.products;
  }

  setProducts(products: Product[]) {
    this.products = products;
  }

  fetchProducts(): Observable<Product[]> {
    return this.http.get<Product[]>('https://angulartest-1f024-default-rtdb.firebaseio.com/products.json');
  }

  addProduct(data: Product): Observable<any> {
    return this.http.post('https://angulartest-1f024-default-rtdb.firebaseio.com/products.json', data);
  }

  setLoading() {
    this.isLoading = !this.isLoading;
  }

  setError(error: string) {
    this.error = error;
  }
}
