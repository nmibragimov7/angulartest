import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { Product } from './products.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items: Product[] = [];
  error!: string;
  isLoading: boolean = false;

  constructor(
    private http: HttpClient
  ) { }

  fetchItems(): Observable<any> {
    return this.http.get('https://angulartest-1f024-default-rtdb.firebaseio.com/carts.json')
  }

  addItem(data: any): Observable<any> {
    const productId = data.id;
    data = {...data, productId};
    delete data.id;
    return this.http.post('https://angulartest-1f024-default-rtdb.firebaseio.com/carts.json', data);
  }

  getItems() {
    return this.items;
  }

  setItems(data: Product[]) {
    this.items = data;
  }

  setLoading() {
    this.isLoading = !this.isLoading;
  }

  setError(error: string) {
    this.error = error;
  }

  removeItem(id: string): Observable<any> {
    // let index = this.items.findIndex(item => item.id == id);
    // this.items.splice(index, 1);

    return this.http.delete(`https://angulartest-1f024-default-rtdb.firebaseio.com/carts/${id}.json`);
  }
}
