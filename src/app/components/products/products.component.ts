import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { ProductsService } from '../../services/products.service';
import { Product } from '../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
  error: string = '';
  isLoading!: boolean;

  constructor(
    public productsService: ProductsService,
  ) { }

  ngOnInit(): void {
    this.productsService.setLoading();
    this.isLoading = this.productsService.isLoading;
    this.productsService.fetchProducts()
      .pipe(map((item: any) => {
        let products: any = [];
        Object.keys(item).forEach((key: string) => {
          products.push({
            id: key,
            userId: item[key].userId,
            title: item[key].title,
            body: item[key].body
          })
        })
        return products;
      }))
      .subscribe(
        (response: any) => {
          this.productsService.setProducts(response);
          this.products = this.productsService.getProducts();
          this.productsService.setError('');
        },
        (error) => {
          this.productsService.setError(error);
          this.error = this.productsService.error;
          this.productsService.setLoading();
          this.isLoading = this.productsService.isLoading;
        },
        () => {
          this.productsService.setLoading();
          this.isLoading = this.productsService.isLoading;
        }
      );
  }

  onShow(title: string) {
    window.alert("Product name: " + title)
  }

}
