import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductsService } from '../../services/products.service';
import { CartService } from '../../services/cart.service';
import { Product } from '../../services/products.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product: Product|undefined;
  products: Product[] = this.productsService.getProducts();
  error: string = '';

  constructor(
    private route: ActivatedRoute,
    public cartService: CartService,
    public productsService: ProductsService,
    public toastService: ToastService
  ) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const idFromRoute = routeParams.get('id');
    this.product = this.products.find(product => product.id === idFromRoute);
  }

  addToCart(product: Product) {
    this.toastService.remove("to-cart");
    this.cartService.setLoading();
    this.cartService.addItem(product)
      .subscribe((response: any) => {
          this.toastService.show("to-cart", "Продукт добавлен в корзину", { classname: 'bg-success text-light', delay: 5000 });
          this.cartService.setError('');
        },
        (error) => {
          this.cartService.setError(error);
          this.error = this.cartService.error;
          this.cartService.setLoading();
        },
        () => {
          this.cartService.setLoading();
        }
      );
  }
}
