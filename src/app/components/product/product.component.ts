import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

import { Product } from '../../services/products.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {
  @Input() product!: Product;
  @Output() show: EventEmitter <string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

}
