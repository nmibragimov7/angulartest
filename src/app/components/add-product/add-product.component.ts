import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Product, ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  form!: FormGroup;
  products: Product[] = this.productsService.getProducts();
  error: string = '';

  constructor(
    public productsService: ProductsService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      userId: new FormControl(null, Validators.required),
      title: new FormControl(null, Validators.required),
      body: new FormControl(null, Validators.required)
    });
  }

  submit() {
    const {id, userId, title, body} = this.form.value;
    this.productsService.setLoading();
    this.productsService.addProduct(this.form.value)
      .subscribe((response: any) => {
          this.productsService.setError('');
          this.router.navigate(['/']);
        },
      (error) => {
          this.productsService.setError(error);
          this.error = this.productsService.error;
          this.productsService.setLoading();
        },
      () => {
          this.productsService.setLoading();
        }
      );
  }
}
