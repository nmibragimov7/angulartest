import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { CartService } from '../../services/cart.service';
import { Product } from '../../services/products.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  items: Product[] = [];
  error: string = '';
  isLoading!: boolean;

  constructor(
    public cartService: CartService,
    public toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.getItems();
  }

  getItems(): void {
    this.cartService.setLoading();
    this.isLoading = this.cartService.isLoading;
    this.cartService.fetchItems()
      .pipe(map((item: any) => {
        let carts: any = [];
        Object.keys(item).forEach((key: string) => {
          carts.push({
            id: key,
            userId: item[key].userId,
            title: item[key].title,
            body: item[key].body,
            productId: item[key].productId
          })
        })
        return carts;
      }))
      .subscribe(
        (response: any) => {
          this.cartService.setItems(response);
          this.items = this.cartService.getItems();
          this.cartService.setError('');
        },
        (error: string) => {
          this.cartService.setError(error);
          this.error = this.cartService.error;
          this.cartService.setLoading();
          this.isLoading = this.cartService.isLoading;
        },
        () => {
          this.cartService.setLoading();
          this.isLoading = this.cartService.isLoading;
        }
      )
  }

  removeItem(id: string) {
    this.toastService.remove("remove-from-cart");
    this.cartService.setLoading();
    this.isLoading = this.cartService.isLoading;
    this.cartService.removeItem(id)
      .subscribe(
        (response: any) => {
          this.toastService.show("remove-from-cart", "Продукт удален из корзины", { classname: 'bg-success text-light', delay: 5000 });
          this.getItems();
          this.cartService.setError('');
        },
        (error) => {
          this.cartService.setError(error);
          this.cartService.setLoading();
          this.isLoading = this.cartService.isLoading;
        },
        () => {
          this.cartService.setLoading();
          this.isLoading = this.cartService.isLoading;
        }
      );
  }
}
