import {Component, TemplateRef, OnDestroy, Input, OnInit} from '@angular/core';
import { ToastService } from '../../../services/toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css']
})
export class ToastComponent implements OnDestroy, OnInit {
  @Input() id!: string;
  constructor(
    public toastService: ToastService
  ) {}

  ngOnInit() {
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }
  }

  isTemplate(toast: any) {
    return toast.textOrTpl instanceof TemplateRef;
  }

  ngOnDestroy() {
    this.toastService.remove(this.id);
  }
}
